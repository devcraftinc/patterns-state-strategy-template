﻿// Copyright 2018-2018 DevCraft, Inc
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Behaviors
{
  [TestClass]
  public class DownloadTokensBehaviors
  {
    private bool permission;
    private TokenBank tokenBank;

    [TestInitialize]
    public void Setup()
    {
      tokenBank = new TokenBank();
    }

    [TestMethod]
    public void CannotDownloadWithoutAnyTokens()
    {
      GivenDownloadTokens(0);

      WhenRequestDownloadPermission();

      ThenPermissionDenied();
    }

    [TestMethod]
    public void CanReturnTokenOnDownload()
    {
      GivenDownloadTokens(0);

      WhenReturnDownload();

      ThenDownloadTokensAre(1);
    }

    [TestMethod]
    public void CannotReturnWhenTokensAreFull()
    {
      ReturningDownloadWhenTokensFull(0);
      ReturningDownloadWhenTokensFull(1);
      ReturningDownloadWhenTokensFull(6);
    }

    [TestMethod]
    public void DownloadingConsumesAToken()
    {
      DownloadingConsumesAToken(3);
      DownloadingConsumesAToken(2);
      DownloadingConsumesAToken(1);
    }

    [TestMethod]
    public void CannotDownloadWhenAccountIsLocked()
    {
      CannotDownloadWhenAccountIsLocked(3);
      CannotDownloadWhenAccountIsLocked(2);
      CannotDownloadWhenAccountIsLocked(1);
    }

    [TestMethod]
    public void ReturningTokensHasNoEffectOnLockedAccount()
    {
      ReturningTokensHasNotEffectOnLockedAccount(0);
      ReturningTokensHasNotEffectOnLockedAccount(1);
      ReturningTokensHasNotEffectOnLockedAccount(2);
    }

    [TestMethod]
    public void RegeneratingReplenishesTokensForTheWeek()
    {
      RegeneratingReplenishesTokens(0);
      RegeneratingReplenishesTokens(1);
      RegeneratingReplenishesTokens(2);
    }

    [TestMethod]
    public void RegeneratingWhenFullBanksAToken()
    {
      RegeneratingWhenfullBanksAToken(0);
      RegeneratingWhenfullBanksAToken(4);
    }

    [TestMethod]
    public void ReplenishingTokensHasNoEffectOnLockedAccount()
    {
      ReplenishingTokensHasNoEffectOnLockedAccount(0, 0);
      ReplenishingTokensHasNoEffectOnLockedAccount(0, 1);
      ReplenishingTokensHasNoEffectOnLockedAccount(1, 1);
      ReplenishingTokensHasNoEffectOnLockedAccount(2, 1);
    }

    [TestMethod]
    public void WithdrawingATokenAddsOne()
    {
      WithdrawingATokenAddsOne(2, 5);
      WithdrawingATokenAddsOne(1, 1);
    }

    [TestMethod]
    public void CannotWithdrawFromEmptyBank()
    {
      CannotWithdrawPastLimits(0, 0);
      CannotWithdrawPastLimits(1, 0);
      CannotWithdrawPastLimits(2, 0);
    }

    [TestMethod]
    public void CannotWithdrawIntoFullTokenPool()
    {
      CannotWithdrawPastLimits(3, 1);
      CannotWithdrawPastLimits(3, 9);
    }

    private void ReturningTokensHasNotEffectOnLockedAccount(int tokens)
    {
      GivenDownloadTokens(tokens);
      GivenAccountIsLocked();

      WhenReturnDownload();

      ThenDownloadTokensAre(tokens);
    }

    private void CannotDownloadWhenAccountIsLocked(int tokens)
    {
      GivenDownloadTokens(tokens);
      GivenAccountIsLocked();

      WhenRequestDownloadPermission();

      ThenPermissionDenied();
      ThenDownloadTokensAre(tokens);
    }

    private void GivenAccountIsLocked()
    {
      Inconclusive();
    }

    private void ReplenishingTokensHasNoEffectOnLockedAccount(int tokens, int banked)
    {
      GivenDownloadTokens(tokens);
      GivenBankedTokens(banked);
      GivenAccountIsLocked();

      WhenTokensReplenished();

      ThenDownloadTokensAre(tokens);
      ThenBankedTokensAre(banked);
    }

    private void ReturningDownloadWhenTokensFull(int bankedTokenCount)
    {
      GivenDownloadTokens(3);
      GivenBankedTokens(bankedTokenCount);

      WhenReturnDownload();

      ThenDownloadTokensAre(3);
      ThenBankedTokensAre(bankedTokenCount);
    }

    private void CannotWithdrawPastLimits(int tokenCount, int bankedTokenCount)
    {
      GivenDownloadTokens(tokenCount);
      GivenBankedTokens(bankedTokenCount);

      WhenTokenIsWithdrawnFromBank();

      ThenDownloadTokensAre(tokenCount);
      ThenBankedTokensAre(bankedTokenCount);
    }

    private void WithdrawingATokenAddsOne(int initialTokens, int initialBankedTokens)
    {
      GivenDownloadTokens(initialTokens);
      GivenBankedTokens(initialBankedTokens);

      WhenTokenIsWithdrawnFromBank();

      ThenDownloadTokensAre(initialTokens + 1);
      ThenBankedTokensAre(initialBankedTokens - 1);
    }

    private void DownloadingConsumesAToken(int initialTokens)
    {
      GivenDownloadTokens(initialTokens);

      WhenRequestDownloadPermission();

      ThenPermissionGranted();
      ThenDownloadTokensAre(initialTokens - 1);
    }

    private void RegeneratingReplenishesTokens(int initialTokens)
    {
      GivenDownloadTokens(initialTokens);

      WhenTokensReplenished();

      ThenDownloadTokensAre(3);
    }

    private void RegeneratingWhenfullBanksAToken(int initialBankedTokens)
    {
      GivenDownloadTokens(3);
      GivenBankedTokens(initialBankedTokens);

      WhenTokensReplenished();

      ThenDownloadTokensAre(3);
      ThenBankedTokensAre(initialBankedTokens + 1);
    }

    private void GivenBankedTokens(int tokenCount)
    {
      tokenBank.TokenCount = tokenCount;
    }

    private void GivenDownloadTokens(int tokenCount)
    {
      Inconclusive();
    }

    private void WhenRequestDownloadPermission()
    {
      Inconclusive();
    }

    private void WhenReturnDownload()
    {
      Inconclusive();
    }

    private void WhenTokensReplenished()
    {
      Inconclusive();
    }

    private void WhenTokenIsWithdrawnFromBank()
    {
      Inconclusive();
    }

    private void ThenPermissionDenied()
    {
      Assert.AreEqual(false, permission);
    }

    private void ThenDownloadTokensAre(int tokenCount)
    {
      Inconclusive();
    }

    private void ThenPermissionGranted()
    {
      Assert.AreEqual(true, permission);
    }

    private void ThenBankedTokensAre(int tokenCount)
    {
      Assert.AreEqual(tokenCount, tokenBank.TokenCount);
    }

    private void Inconclusive()
    {
      Assert.Inconclusive("This binding not implemented.");
    }
  }
}