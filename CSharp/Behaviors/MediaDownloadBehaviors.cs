﻿// Copyright 2018-2018 DevCraft, Inc
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System.Collections.Generic;
using System.Linq;
using Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Behaviors
{
  [TestClass]
  public class MediaDownloadBehaviors
  {
    private MockDestination downloadTarget;
    private Page[] pages;

    [TestInitialize]
    public void Setup()
    {
      GivenPages(5);
    }

    [TestMethod]
    public void BooksCannotBeDownloadedToComputersBecauseRightsManagementIsTooWeak()
    {
      GivenMediaItemIsABook();
      GivenDownloadTargetIsAComputer();

      WhenMediaItemIsDownloaded();

      ThenPermissionIsDenied();
      ThenPagesDownloaded();
    }

    [TestMethod]
    public void BooksCanBeDownloadedToMobileAppsBecauseRightsManagementIsSufficient()
    {
      GivenMediaItemIsABook();
      GivenDownloadTargetIsAMobileDevice();

      WhenMediaItemIsDownloaded();

      ThenPermissionIsGranted();
      ThenPagesDownloaded(pages);
    }

    [TestMethod]
    public void BooksCanBeStreamedToAnythingBecauseRightsManagementIsSufficient()
    {
      GivenMediaItemIsABook();
      GivenDownloadTargetIsAnyDevice();

      WhenStreamingIsInitiated();

      ThenPermissionIsGranted();
      ThenPagesDownloaded(First(3, pages));
    }

    [TestMethod]
    public void NonProfessionalPeriodicalsCanBeDownloadedToAnythingBecauseRightsManagementIsNotAnIssue()
    {
      GivenMediaItemIsANonProfessionalPeriodical();
      GivenDownloadTargetIsAnyDevice();

      WhenMediaItemIsDownloaded();

      ThenPermissionIsGranted();
      ThenPagesDownloaded(pages);
    }

    [TestMethod]
    public void NonProfessionalPeriodicalsCanBeStreamedToAnythingBecauseRightsManagementIsNotAnIssue()
    {
      GivenMediaItemIsANonProfessionalPeriodical();
      GivenDownloadTargetIsAnyDevice();

      WhenStreamingIsInitiated();

      ThenPermissionIsGranted();
      ThenPagesDownloaded(First(5, pages));
    }

    [TestMethod]
    public void ProfessionalPeriodicalsCanBeDownloadedToComputersBecauseRightsManagementIsNotAnIssue()
    {
      GivenMediaItemIsAProfessionalPeriodical();
      GivenDownloadTargetIsAComputer();

      WhenMediaItemIsDownloaded();

      ThenPermissionIsGranted();
      ThenPagesDownloaded(pages);
    }

    [TestMethod]
    public void
      ProfessionalPeriodicalsCannotBeDownloadedToMobileDevicesBecauseTheyAreTooDetailedAndRequireARealComputer()
    {
      GivenMediaItemIsAProfessionalPeriodical();
      GivenDownloadTargetIsAMobileDevice();

      WhenMediaItemIsDownloaded();

      ThenPermissionIsDenied();
      ThenPagesDownloaded();
    }

    [TestMethod]
    public void ProfessionalPeriodicalsCannotBeStreamedToAnythingBecauseTheyareLargeAndDetailedReferenceMaterials()
    {
      GivenMediaItemIsAProfessionalPeriodical();
      GivenDownloadTargetIsAnyDevice();

      WhenStreamingIsInitiated();

      ThenPermissionIsDenied();
      ThenPagesDownloaded();
    }

    [TestMethod]
    public void StreamingLimitedToNumberOfPages()
    {
      pages = new[] {new Page()};
      GivenMediaItemIsANonProfessionalPeriodical();
      GivenDownloadTargetIsAnyDevice();

      WhenStreamingIsInitiated();

      ThenPermissionIsGranted();
      ThenPagesDownloaded(pages);
    }

    [TestMethod]
    public void BookStreamingIsVeryOnDemandBecauseBookPagesAreFast()
    {
      BookStreamBufferingCase(5, 3, 3, 0);
      BookStreamBufferingCase(5, 3, 2, 1);
      BookStreamBufferingCase(5, 3, 1, 2);
    }

    [TestMethod]
    public void BookStreamingDoesNotGoPastEnd()
    {
      BookStreamBufferingCase(5, 3, 0, 2);
    }

    [TestMethod]
    public void StreamedPeriodicalsAlwaysKeepBufferAtAMinimumOfFiveBecauseTheySometimesHaveHeavyPicturesInThem()
    {
      PeriodicalStreamBufferingCase(20, 5, 3, 2);
      PeriodicalStreamBufferingCase(20, 5, 2, 3);
      PeriodicalStreamBufferingCase(20, 5, 1, 4);
      PeriodicalStreamBufferingCase(20, 5, 0, 5);
    }

    [TestMethod]
    public void StreamedPeriodicalsAttemptToExpandBufferToTenToEvenOutCostOfHeavyPictures()
    {
      PeriodicalStreamBufferingCase(20, 5, 4, 2);
      PeriodicalStreamBufferingCase(20, 5, 8, 2);
      PeriodicalStreamBufferingCase(20, 5, 9, 1);
      PeriodicalStreamBufferingCase(20, 5, 10, 0);
    }

    private void BookStreamBufferingCase(int pageCount, int streamed, int bufferedPageCount, int expected)
    {
      GivenPages(pageCount);
      GivenMediaItemIsABook();
      GivenDownloadTargetIsAnyDevice();

      WhenStreamingBufferIsUpdated(streamed, bufferedPageCount);

      ThenPagesDownloaded(RangeOf(streamed, streamed + expected, pages));
    }

    private void PeriodicalStreamBufferingCase(int pageCount, int streamed, int bufferedPageCount, int expected)
    {
      GivenPages(pageCount);
      GivenMediaItemIsANonProfessionalPeriodical();
      GivenDownloadTargetIsAnyDevice();

      WhenStreamingBufferIsUpdated(streamed, bufferedPageCount);

      ThenPagesDownloaded(RangeOf(streamed, streamed + expected, pages));
    }

    private void WhenStreamingBufferIsUpdated(int streamed, int bufferedPageCount)
    {
      Inconclusive();
    }

    private Page[] RangeOf(int lower, int upper, Page[] pages)
    {
      var result = new List<Page>();
      for (var i = lower; i < upper; ++i)
        result.Add(pages[i]);

      return result.ToArray();
    }

    private void GivenPages(int count)
    {
      var temp = new List<Page>();

      for (var i = 0; i < count; ++i)
        temp.Add(new Page());

      pages = temp.ToArray();
    }

    private void GivenMediaItemIsABook()
    {
      Inconclusive();
    }

    private void GivenMediaItemIsANonProfessionalPeriodical()
    {
      Inconclusive();
    }

    private void GivenMediaItemIsAProfessionalPeriodical()
    {
      Inconclusive();
    }

    private void WhenStreamingIsInitiated()
    {
      Inconclusive();
    }

    private void GivenDownloadTargetIsAComputer()
    {
      Inconclusive();
    }

    private void GivenDownloadTargetIsAMobileDevice()
    {
      Inconclusive();
    }

    private void GivenDownloadTargetIsAnyDevice()
    {
      Inconclusive();
    }

    private void WhenMediaItemIsDownloaded()
    {
      Inconclusive();
    }

    private void ThenPermissionIsDenied()
    {
      Assert.AreEqual(true, downloadTarget.DownloadWasDenied);
    }

    private void ThenPermissionIsGranted()
    {
      Assert.AreEqual(false, downloadTarget.DownloadWasDenied);
    }

    private void ThenPagesDownloaded(params Page[] expected)
    {
      CollectionAssert.AreEqual(expected, downloadTarget.PushedPages);
    }

    private static void Inconclusive()
    {
      Assert.Inconclusive("This binding not implemented, yet.");
    }

    private Page[] First(int count, Page[] testPages) => testPages.Take(count).ToArray();
  }
}