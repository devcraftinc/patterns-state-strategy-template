// Copyright 2018-2018 DevCraft, Inc
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package sessions;


import org.junit.*;

import static junit.framework.TestCase.assertSame;

public class DowngradeImagesBehaviors {
    private MockDestination destination;
    private Image downgradedImage;
    private MockImageDowngrader downgrader;
    private Image image;
    private SessionType sessionType;

    @Before
    public void setup() {
        downgrader = new MockImageDowngrader();
        destination = new MockDestination(DestinationType.Computer);
    }

    @Test
    public void downgradeForAnyImageForWeakConnectionsButLeaveFullImageAsUpgrade() {
        givenImageOfAnyQuality();
        givenDowngradedImage(ImageQuality.Medium);
        givenWeakConnection();

        whenPageIsFetched();

        thenDownloadIncludedImage(downgradedImage);
        thenDownloadDidNotIncludeImage(image);
        thenImageHasUpgrade(downgradedImage, image);
    }

    @Test
    public void downgradeForFreePreviews() {
        givenImageOfAnyQuality();
        givenDowngradedImage(ImageQuality.Low);
        givenUnpaidAccount();

        whenPageIsFetched();

        thenDownloadIncludedImage(downgradedImage);
        thenDownloadDidNotIncludeImage(image);
        thenImageHasNoUpgrade(downgradedImage);
    }

    @Test
    public void upToHighResolutionImageSentAsFullVersionForStrongConnectionWithPaidAccount() {
        givenImageOfQuality(ImageQuality.High);
        givenNormalConnectionAndPaidAccount();

        whenPageIsFetched();

        thenDownloadIncludedImage(image);
        thenImageHasNoUpgrade(image);
    }

    @Test
    public void ultraHighResolutionImageSentAsHighResolutionImageWithUpgradeForStrongConnectionAndPaidAccount() {
        givenImageOfQuality(ImageQuality.UltraHigh);
        givenDowngradedImage(ImageQuality.High);
        givenNormalConnectionAndPaidAccount();

        whenPageIsFetched();

        thenDownloadIncludedImage(downgradedImage);
        thenDownloadDidNotIncludeImage(image);
        thenImageHasUpgrade(downgradedImage, image);
    }

    @Test
    public void doesNotRegisterUpgradesAfterPagePush() {
        givenImageOfAnyQuality();
        givenDowngradedImage(ImageQuality.Medium);
        givenWeakConnection();
        givenPushingPageBlocksUpgrade();

        whenPageIsFetched();
    }

    private void givenNormalConnectionAndPaidAccount() {
        sessionType = SessionType.Normal;
    }

    private void givenWeakConnection() {
        sessionType = SessionType.WeakConnection;
    }

    private void givenUnpaidAccount() {
        sessionType = SessionType.FreeTrial;
    }

    private void givenPushingPageBlocksUpgrade() {
        destination.setBeforePushPage(() -> destination.setBeforeRegisterUpgrade(Assert::fail));
    }

    private void givenImageOfQuality(ImageQuality quality) {
        image = new Image(quality);
    }

    private void givenImageOfAnyQuality() {
        image = new Image(Any.enumerationValue(ImageQuality.class));
    }

    private void givenDowngradedImage(ImageQuality quality) {
        downgradedImage = new Image();
        downgrader.setupDowngrade(image, quality, downgradedImage);
    }

    private void whenPageIsFetched() {
        inconclusive();
    }

    private void thenDownloadIncludedImage(Image expected) {
        for (Page page : destination.getPushedPages())
            for (Image image : page.getImages())
                if (image == expected)
                    return;

        Assert.fail();
    }

    private void thenDownloadDidNotIncludeImage(Image expected) {
        for (Page page : destination.getPushedPages())
            for (Image image : page.getImages())
                if (image == expected)
                    Assert.fail();
    }

    private void thenImageHasNoUpgrade(Image downgradedImage) {
        Assert.assertNull(destination.GetImageUpgrade(downgradedImage));
    }

    private void thenImageHasUpgrade(Image downgraded, Image upgraded) {
        assertSame(upgraded, destination.GetImageUpgrade(downgraded));
    }

    private static void inconclusive() {
        Assert.fail("These bindings are not implemented.");
    }
}

