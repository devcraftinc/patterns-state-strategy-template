// Copyright 2018-2018 DevCraft, Inc
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package sessions;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class DownloadTokensBehaviors {
    private boolean permission;
    private TokenBank tokenBank;

    @Before
    public void setup() {
        tokenBank = new TokenBank();
    }

    @Test
    public void cannotDownloadWithoutAnyTokens() {
        givenDownloadTokens(0);

        whenRequestDownloadPermission();

        thenPermissionDenied();
    }

    @Test
    public void canReturnTokenOnDownload() {
        givenDownloadTokens(0);

        whenReturnDownload();

        thenDownloadTokensAre(1);
    }

    @Test
    public void cannotReturnWhenTokensAreFull() {
        returningDownloadWhenTokensFull(0);
        returningDownloadWhenTokensFull(1);
        returningDownloadWhenTokensFull(6);
    }

    @Test
    public void downloadingConsumesAToken() {
        downloadingConsumesAToken(3);
        downloadingConsumesAToken(2);
        downloadingConsumesAToken(1);
    }

    @Test
    public void cannotDownloadWhenAccountIsLocked() {
        cannotdownloadwhenaccountislocked(3);
        cannotdownloadwhenaccountislocked(2);
        cannotdownloadwhenaccountislocked(1);
    }

    @Test
    public void returningTokensHasNoEffectOnLockedAccount() {
        returningTokensHasNotEffectOnLockedAccount(0);
        returningTokensHasNotEffectOnLockedAccount(1);
        returningTokensHasNotEffectOnLockedAccount(2);
    }

    @Test
    public void regeneratingReplenishesTokensForTheWeek() {
        regeneratingReplenishesTokens(0);
        regeneratingReplenishesTokens(1);
        regeneratingReplenishesTokens(2);
    }

    @Test
    public void regeneratingWhenFullBanksAToken() {
        regeneratingWhenfullBanksAToken(0);
        regeneratingWhenfullBanksAToken(4);
    }

    @Test
    public void replenishingTokensHasNoEffectOnLockedAccount() {
        replenishingTokensHasNoEffectOnLockedAccount(0, 0);
        replenishingTokensHasNoEffectOnLockedAccount(0, 1);
        replenishingTokensHasNoEffectOnLockedAccount(1, 1);
        replenishingTokensHasNoEffectOnLockedAccount(2, 1);
    }

    @Test
    public void withdrawingATokenAddsOne() {
        withdrawingATokenAddsOne(2, 5);
        withdrawingATokenAddsOne(1, 1);
    }

    @Test
    public void cannotWithdrawFromEmptyBank() {
        cannotWithdrawPastLimits(0, 0);
        cannotWithdrawPastLimits(1, 0);
        cannotWithdrawPastLimits(2, 0);
    }

    @Test
    public void cannotWithdrawIntoFullTokenPool() {
        cannotWithdrawPastLimits(3, 1);
        cannotWithdrawPastLimits(3, 9);
    }

    private void returningTokensHasNotEffectOnLockedAccount(int tokens) {
        givenDownloadTokens(tokens);
        givenAccountIsLocked();

        whenReturnDownload();

        thenDownloadTokensAre(tokens);
    }

    private void cannotdownloadwhenaccountislocked(int tokens) {
        givenDownloadTokens(tokens);
        givenAccountIsLocked();

        whenRequestDownloadPermission();

        thenPermissionDenied();
        thenDownloadTokensAre(tokens);
    }

    private void givenAccountIsLocked() {
        inconclusive();
    }

    private void replenishingTokensHasNoEffectOnLockedAccount(int tokens, int banked) {
        givenDownloadTokens(tokens);
        givenBankedTokens(banked);
        givenAccountIsLocked();

        whenTokensReplenished();

        thenDownloadTokensAre(tokens);
        thenBankedTokensAre(banked);
    }

    private void returningDownloadWhenTokensFull(int bankedTokenCount) {
        givenDownloadTokens(3);
        givenBankedTokens(bankedTokenCount);

        whenReturnDownload();

        thenDownloadTokensAre(3);
        thenBankedTokensAre(bankedTokenCount);
    }

    private void cannotWithdrawPastLimits(int tokenCount, int bankedTokenCount) {
        givenDownloadTokens(tokenCount);
        givenBankedTokens(bankedTokenCount);

        whenTokenIsWithdrawnFromBank();

        thenDownloadTokensAre(tokenCount);
        thenBankedTokensAre(bankedTokenCount);
    }

    private void withdrawingATokenAddsOne(int initialTokens, int initialBankedTokens) {
        givenDownloadTokens(initialTokens);
        givenBankedTokens(initialBankedTokens);

        whenTokenIsWithdrawnFromBank();

        thenDownloadTokensAre(initialTokens + 1);
        thenBankedTokensAre(initialBankedTokens - 1);
    }

    private void downloadingConsumesAToken(int initialTokens) {
        givenDownloadTokens(initialTokens);

        whenRequestDownloadPermission();

        thenPermissionGranted();
        thenDownloadTokensAre(initialTokens - 1);
    }

    private void regeneratingReplenishesTokens(int initialTokens) {
        givenDownloadTokens(initialTokens);

        whenTokensReplenished();

        thenDownloadTokensAre(3);
    }

    private void regeneratingWhenfullBanksAToken(int initialBankedTokens) {
        givenDownloadTokens(3);
        givenBankedTokens(initialBankedTokens);

        whenTokensReplenished();

        thenDownloadTokensAre(3);
        thenBankedTokensAre(initialBankedTokens + 1);
    }

    private void givenBankedTokens(int tokenCount) {
        tokenBank.setTokenCount(tokenCount);
    }

    private void givenDownloadTokens(int tokenCount) {
        inconclusive();
    }

    private void whenRequestDownloadPermission() {
        inconclusive();
    }

    private void whenReturnDownload() {
        inconclusive();
    }

    private void whenTokensReplenished() {
        inconclusive();
    }

    private void whenTokenIsWithdrawnFromBank() {
        inconclusive();
    }

    private void thenPermissionDenied() {
        assertEquals(false, permission);
    }

    private void thenDownloadTokensAre(int tokenCount) {
        inconclusive();
    }

    private void thenPermissionGranted() {
        assertEquals(true, permission);
    }

    private void thenBankedTokensAre(int tokenCount) {
        assertEquals(tokenCount, tokenBank.getTokenCount());
    }

    private void inconclusive() {
        Assert.fail("This binding not implemented.");
    }
}

