// Copyright 2018-2018 DevCraft, Inc
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package sessions;

import java.util.*;

class MockDestination implements Destination {
    private final List<Page> pushedPages = new ArrayList<>();
    private Map<Image, Image> upgrades = new HashMap<>();

    MockDestination(DestinationType type) {
        this.type = type;
    }

    private Runnable beforeRegisterUpgrade;

    public void setBeforeRegisterUpgrade(Runnable value) {
        beforeRegisterUpgrade = value;
    }

    private Runnable beforePushPage;

    public void setBeforePushPage(Runnable value) {
        beforePushPage = value;
    }

    private boolean downloadWasDenied;

    public boolean getDownloadWasDenied() {
        return downloadWasDenied;
    }

    public Page[] getPushedPages() {
        return pushedPages.toArray(new Page[0]);
    }

    private DestinationType type;

    public DestinationType getType() {
        return type;
    }

    public void downloadDenied() {
        downloadWasDenied = true;
    }

    public void pushPage(Page page) {
        if (beforePushPage != null)
            beforePushPage.run();

        pushedPages.add(page);
    }

    public void registerUpgrade(Image downgraded, Image upgraded) {
        if (beforeRegisterUpgrade != null)
            beforeRegisterUpgrade.run();

        upgrades.put(downgraded, upgraded);
    }

    public Image GetImageUpgrade(Image downgraded) {
        if (!upgrades.containsKey(downgraded))
            return null;

        return upgrades.get(downgraded);
    }
}

