// Copyright 2018-2018 DevCraft, Inc
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package sessions;

import org.junit.*;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class MediaDownloadBehaviors {
    private MockDestination downloadTarget;
    private Page[] pages;

    @Before
    public void Setup() {
        givenPages(5);
    }

    @Test
    public void booksCannotBeDownloadedToComputersBecauseRightsManagementIsTooWeak() {
        givenMediaItemIsABook();
        givenDownloadTargetIsAComputer();

        whenMediaItemIsDownloaded();

        thenPermissionIsDenied();
        thenPagesDownloaded();
    }

    @Test
    public void booksCanBeDownloadedToMobileAppsBecauseRightsManagementIsSufficient() {
        givenMediaItemIsABook();
        givenDownloadTargetIsAMobileDevice();

        whenMediaItemIsDownloaded();

        thenPermissionIsGranted();
        thenPagesDownloaded(pages);
    }

    @Test
    public void booksCanBeStreamedToAnythingBecauseRightsManagementIsSufficient() {
        givenMediaItemIsABook();
        givenDownloadTargetIsAnyDevice();

        whenStreamingIsInitiated();

        thenPermissionIsGranted();
        thenPagesDownloaded(first(3, pages));
    }

    @Test
    public void nonProfessionalPeriodicalsCanBeDownloadedToAnythingBecauseRightsManagementIsNotAnIssue() {
        givenMediaItemIsANonProfessionalPeriodical();
        givenDownloadTargetIsAnyDevice();

        whenMediaItemIsDownloaded();

        thenPermissionIsGranted();
        thenPagesDownloaded(pages);
    }

    @Test
    public void nonProfessionalPeriodicalsCanBeStreamedToAnythingBecauseRightsManagementIsNotAnIssue() {
        givenMediaItemIsANonProfessionalPeriodical();
        givenDownloadTargetIsAnyDevice();

        whenStreamingIsInitiated();

        thenPermissionIsGranted();
        thenPagesDownloaded(first(5, pages));
    }

    @Test
    public void professionalPeriodicalsCanBeDownloadedToComputersBecauseRightsManagementIsNotAnIssue() {
        givenMediaItemIsAProfessionalPeriodical();
        givenDownloadTargetIsAComputer();

        whenMediaItemIsDownloaded();

        thenPermissionIsGranted();
        thenPagesDownloaded(pages);
    }

    @Test
    public void
    professionalPeriodicalsCannotBeDownloadedToMobileDevicesBecauseTheyAreTooDetailedAndRequireARealComputer() {
        givenMediaItemIsAProfessionalPeriodical();
        givenDownloadTargetIsAMobileDevice();

        whenMediaItemIsDownloaded();

        thenPermissionIsDenied();
        thenPagesDownloaded();
    }

    @Test
    public void professionalPeriodicalsCannotBeStreamedToAnythingBecauseTheyareLargeAndDetailedReferenceMaterials() {
        givenMediaItemIsAProfessionalPeriodical();
        givenDownloadTargetIsAnyDevice();

        whenStreamingIsInitiated();

        thenPermissionIsDenied();
        thenPagesDownloaded();
    }

    @Test
    public void streamingLimitedToNumberOfPages() {
        pages = new Page[]{
                new Page()
        };
        givenMediaItemIsANonProfessionalPeriodical();
        givenDownloadTargetIsAnyDevice();

        whenStreamingIsInitiated();

        thenPermissionIsGranted();
        thenPagesDownloaded(pages);
    }

    @Test
    public void bookStreamingIsVeryOnDemandBecauseBookPagesAreFast() {
        bookStreamBufferingCase(5, 3, 3, 0);
        bookStreamBufferingCase(5, 3, 2, 1);
        bookStreamBufferingCase(5, 3, 1, 2);
    }

    @Test
    public void bookStreamingDoesNotGoPastEnd() {
        bookStreamBufferingCase(5, 3, 0, 2);
    }

    @Test
    public void streamedPeriodicalsAlwaysKeepBufferAtAMinimumOfFiveBecauseTheySometimesHaveHeavyPicturesInThem() {
        periodicalStreamBufferingCase(20, 5, 3, 2);
        periodicalStreamBufferingCase(20, 5, 2, 3);
        periodicalStreamBufferingCase(20, 5, 1, 4);
        periodicalStreamBufferingCase(20, 5, 0, 5);
    }

    @Test
    public void streamedPeriodicalsAttemptToExpandBufferToTenToEvenOutCostOfHeavyPictures() {
        periodicalStreamBufferingCase(20, 5, 4, 2);
        periodicalStreamBufferingCase(20, 5, 8, 2);
        periodicalStreamBufferingCase(20, 5, 9, 1);
        periodicalStreamBufferingCase(20, 5, 10, 0);
    }

    private void bookStreamBufferingCase(int pageCount, int streamed, int bufferedPageCount, int expected) {
        givenPages(pageCount);
        givenMediaItemIsABook();
        givenDownloadTargetIsAnyDevice();

        whenStreamingBufferIsUpdated(streamed, bufferedPageCount);

        thenPagesDownloaded(rangeOf(streamed, streamed + expected, pages));
    }

    private void periodicalStreamBufferingCase(int pageCount, int streamed, int bufferedPageCount, int expected) {
        givenPages(pageCount);
        givenMediaItemIsANonProfessionalPeriodical();
        givenDownloadTargetIsAnyDevice();

        whenStreamingBufferIsUpdated(streamed, bufferedPageCount);

        thenPagesDownloaded(rangeOf(streamed, streamed + expected, pages));
    }

    private void whenStreamingBufferIsUpdated(int streamed, int bufferedPageCount) {
        inconclusive();
    }

    private Page[] rangeOf(int lower, int upper, Page[] pages) {
        return Arrays.copyOfRange(pages, lower, upper);
    }

    private void givenPages(int length) {
        List<Page> temp = new ArrayList<>();

        for (int i = 0; i < length; ++i)
            temp.add(new Page());

        pages = temp.toArray(new Page[0]);
    }

    private void givenMediaItemIsABook() {
        inconclusive();
    }

    private void givenMediaItemIsANonProfessionalPeriodical() {
        inconclusive();
    }

    private void givenMediaItemIsAProfessionalPeriodical() {
        inconclusive();
    }

    private void whenStreamingIsInitiated() {
        inconclusive();
    }

    private void givenDownloadTargetIsAComputer() {
        downloadTarget = new MockDestination(DestinationType.Computer);
    }

    private void givenDownloadTargetIsAMobileDevice() {
        downloadTarget = new MockDestination(DestinationType.MobileDevice);
    }

    private void givenDownloadTargetIsAnyDevice() {
        downloadTarget = new MockDestination(Any.enumerationValue(DestinationType.class));
    }

    private void whenMediaItemIsDownloaded() {
        inconclusive();
    }

    private void thenPermissionIsDenied() {
        assertEquals(true, downloadTarget.getDownloadWasDenied());
    }

    private void thenPermissionIsGranted() {
        assertEquals(false, downloadTarget.getDownloadWasDenied());
    }

    private void thenPagesDownloaded(Page... expected) {
        Assert.assertArrayEquals(expected, downloadTarget.getPushedPages());
    }

    private static void inconclusive() {
        Assert.fail("This binding not implemented, yet.");
    }

    private Page[] first(int length, Page[] testPages) {
        return Arrays.copyOfRange(testPages, 0, length);
    }
}

