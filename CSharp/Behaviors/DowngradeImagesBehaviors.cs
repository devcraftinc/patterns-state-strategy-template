﻿// Copyright 2018-2018 DevCraft, Inc
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using Implementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Behaviors
{
  [TestClass]
  public class DowngradeImagesBehaviors
  {
    private MockDestination destination;
    private Image downgradedImage;
    private MockImageDowngrader downgrader;
    private Image image;
    private SessionType sessionType;

    [TestInitialize]
    public void Setup()
    {
      downgrader = new MockImageDowngrader();
      destination = new MockDestination(DestinationType.Computer);
    }

    [TestMethod]
    public void DowngradeForAnyImageForWeakConnectionsButLeaveFullImageAsUpgrade()
    {
      GivenImageOfAnyQuality();
      GivenDowngradedImage(ImageQuality.Medium);
      GivenWeakConnection();

      WhenPageIsFetched();

      ThenDownloadIncludedImage(downgradedImage);
      ThenDownloadDidNotIncludeImage(image);
      ThenImageHasUpgrade(downgradedImage, image);
    }

    [TestMethod]
    public void DowngradeForFreePreviews()
    {
      GivenImageOfAnyQuality();
      GivenDowngradedImage(ImageQuality.Low);
      GivenUnpaidAccount();

      WhenPageIsFetched();

      ThenDownloadIncludedImage(downgradedImage);
      ThenDownloadDidNotIncludeImage(image);
      ThenImageHasNoUpgrade(downgradedImage);
    }

    [TestMethod]
    public void UpToHighResolutionImageSentAsFullVersionForStrongConnectionWithPaidAccount()
    {
      GivenImageOfQuality(ImageQuality.High);
      GivenNormalConnectionAndPaidAccount();

      WhenPageIsFetched();

      ThenDownloadIncludedImage(image);
      ThenImageHasNoUpgrade(image);
    }

    [TestMethod]
    public void UltraHighResolutionImageSentAsHighResolutionImageWithUpgradeForStrongConnectionAndPaidAccount()
    {
      GivenImageOfQuality(ImageQuality.UltraHigh);
      GivenDowngradedImage(ImageQuality.High);
      GivenNormalConnectionAndPaidAccount();

      WhenPageIsFetched();

      ThenDownloadIncludedImage(downgradedImage);
      ThenDownloadDidNotIncludeImage(image);
      ThenImageHasUpgrade(downgradedImage, image);
    }

    [TestMethod]
    public void DoesNotRegisterUpgradesAfterPagePush()
    {
      GivenImageOfAnyQuality();
      GivenDowngradedImage(ImageQuality.Medium);
      GivenWeakConnection();
      GivenPushingPageBlocksUpgrade();

      WhenPageIsFetched();
    }

    private void GivenNormalConnectionAndPaidAccount()
    {
      sessionType = SessionType.Normal;
    }

    private void GivenWeakConnection()
    {
      sessionType = SessionType.WeakConnection;
    }

    private void GivenUnpaidAccount()
    {
      sessionType = SessionType.FreeTrial;
    }

    private void GivenPushingPageBlocksUpgrade()
    {
      destination.BeforePushPage = () => { destination.BeforeRegisterUpgrade = Assert.Fail; };
    }

    private void GivenImageOfQuality(ImageQuality quality)
    {
      image = new Image(quality);
    }

    private void GivenImageOfAnyQuality()
    {
      image = new Image(Any.EnumerationValue<ImageQuality>());
    }

    private void GivenDowngradedImage(ImageQuality quality)
    {
      downgradedImage = new Image();
      downgrader.SetupDowngrade(image, quality, downgradedImage);
    }

    private void WhenPageIsFetched()
    {
      Inconclusive();
    }

    private void ThenDownloadIncludedImage(Image expected)
    {
      foreach (var page in destination.PushedPages)
      foreach (var image in page.Images)
        if (image == expected)
          return;

      Assert.Fail();
    }

    private void ThenDownloadDidNotIncludeImage(Image expected)
    {
      foreach (var page in destination.PushedPages)
      foreach (var image in page.Images)
        if (image == expected)
          Assert.Fail();
    }

    private void ThenImageHasNoUpgrade(Image downgradedImage)
    {
      Assert.IsNull(destination.GetImageUpgrade(downgradedImage));
    }

    private void ThenImageHasUpgrade(Image downgraded, Image upgraded)
    {
      Assert.AreSame(upgraded, destination.GetImageUpgrade(downgraded));
    }

    private static void Inconclusive()
    {
      Assert.Inconclusive("These bindings are not implemented.");
    }
  }
}